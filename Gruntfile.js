module.exports = function (grunt) {
 
    grunt.initConfig({
        watch: {
            files: [
                'src/**/*.js',
                'test/**/*.js'
            ],
            options: {
                spawn: true,
                livereload: true
            }
        }
    });
 
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.registerTask("default", ["watch"]);
};
